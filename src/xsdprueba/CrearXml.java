/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xsdprueba;

import generarXmls.RequestForPayment;
import java.io.File;
import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.xml.sax.SAXException;

/**
 *
 * @author sistemas4
 */
public class CrearXml {
    
    public RequestForPayment crearXml () {
        
        RequestForPayment requestForPayment = new RequestForPayment();
         RequestForPayment.AdditionalInformation additional = new RequestForPayment.AdditionalInformation();
        RequestForPayment.AdditionalInformation.ReferenceIdentification referenceIdentification = new RequestForPayment.AdditionalInformation.ReferenceIdentification();
        referenceIdentification.setValue("nueva referencia");
        referenceIdentification.setType("estring");        
        additional.setReferenceIdentification(referenceIdentification);
        requestForPayment.setAdditionalInformation(additional);
        
        RequestForPayment.Buyer buyer = new RequestForPayment.Buyer();
        buyer.setGln("cadena buyer"); 
        requestForPayment.setBuyer(buyer);
        requestForPayment.setContentVersion("2017-01-01/1.0"); 
        
        RequestForPayment.Currency currency = new RequestForPayment.Currency();
        currency.setCurrencyFunction("currencyFuncion");
        currency.setCurrencyISOCode("isoCode");    
        requestForPayment.setCurrency(currency);
        
        requestForPayment.setDocumentStatus("activo");
        
        RequestForPayment.OrderIdentification orderIdentification = new RequestForPayment.OrderIdentification(); 
        RequestForPayment.OrderIdentification.ReferenceIdentification referenceIdentifications = new RequestForPayment.OrderIdentification.ReferenceIdentification();
        referenceIdentifications.setValue("nueva referencia");
        referenceIdentifications.setType("estring"); 
        orderIdentification.setReferenceIdentification(referenceIdentifications); 
        requestForPayment.setOrderIdentification(orderIdentification);
        
        RequestForPayment.RequestForPaymentIdentification  requestForPaymentIdentification = new RequestForPayment.RequestForPaymentIdentification();
        requestForPaymentIdentification.setEntityType("tipo de entidad");
        requestForPaymentIdentification.setUniqueCreatorIdentification("creador de identificacion");
        requestForPayment.setRequestForPaymentIdentification(requestForPaymentIdentification);
        
        RequestForPayment.Seller seller = new RequestForPayment.Seller();
        RequestForPayment.Seller.AlternatePartyIdentification alternatePartyIdentification = new RequestForPayment.Seller.AlternatePartyIdentification();
        alternatePartyIdentification.setType("cadena");
        alternatePartyIdentification.setValue("descripcion"); 
        seller.setAlternatePartyIdentification(alternatePartyIdentification);
        seller.setGln("seller");
        requestForPayment.setSeller(seller);
        
        RequestForPayment.SpecialInstruction specialInstruction = new RequestForPayment.SpecialInstruction();
        specialInstruction.setCode("codigo"); 
        specialInstruction.setText("descripcion de la instruccion"); 
        requestForPayment.setSpecialInstruction(specialInstruction);
        requestForPayment.setType("cadena");
        
        requestForPayment.setDocumentStructureVersion("documentoStructure"); 
        
        
        return requestForPayment;
    }
    
    
    /**
     * metodo que recibe la ruta del archivo xsd, y del xml para validar que el xml esta generado correctamente.
     * @param xsdPath
     * @param xmlPath
     * @return 
     */
    public static boolean validateXMLSchema(String xsdPath, String xmlPath){
        
        try {
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new File(xsdPath));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new File(xmlPath)));
        } catch (Exception e) {
            System.out.println("Exception: "+e.getMessage());
            return false;
        }
        return true;
    }
    
}
